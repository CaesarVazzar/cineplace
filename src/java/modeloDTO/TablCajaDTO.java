/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modeloDTO;

/**
 *
 * @author Cesar.Gutierrez
 */
public class TablCajaDTO {
    //columna de la base de datos de la tabla Id_Asientos
   private int id_Transaccion;
   private int Tota_Caja;
   private String Form_Pago;
   private int Nume_Boletos;
   private String se_Modifico;
   //Area de variables auxiliares
   
   //Area de constructores

    public TablCajaDTO() {
    }

    public TablCajaDTO(int id_Transaccion) {
        this.id_Transaccion = id_Transaccion;
    }

    public TablCajaDTO(int id_Transaccion, int Tota_Caja, String Form_Pago, int Nume_Boletos, String se_Modifico) {
        this.id_Transaccion = id_Transaccion;
        this.Tota_Caja = Tota_Caja;
        this.Form_Pago = Form_Pago;
        this.Nume_Boletos = Nume_Boletos;
        this.se_Modifico = se_Modifico;
    }
      // area de getter
    public int getid_Transaccion() {
        return id_Transaccion;
    }

    public int getTota_Caja() {
        return Tota_Caja;
    }
    
    public String getForm_Pago() {
        return Form_Pago;
    }
    
    public int getNume_Boletos() {
        return Nume_Boletos;
    }
    
    public String getse_Modifico() {
        return se_Modifico;
    }

    public void setid_Transaccion(int id_Transaccion) {
        this.id_Transaccion = id_Transaccion;
    }

    public void setTota_Caja(int Tota_Caja) {
        this.Tota_Caja = Tota_Caja;
    }
    
    public void setForm_Pago(String Form_Pago) {
        this.Form_Pago = Form_Pago;
    }
    
    public void setNume_Boletos(int Nume_Boletos) {
        this.Nume_Boletos = Nume_Boletos;
    }
    
    public void setse_Modifico(String se_Modifico) {
        this.se_Modifico = se_Modifico;
    }
    
    // Area de otros metodos
  
}
