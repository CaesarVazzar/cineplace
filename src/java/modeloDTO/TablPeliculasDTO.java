/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modeloDTO;


import java.sql.Date;
/**
 *
 * @author vvs
 */
public class TablPeliculasDTO {
   private int id_Peliculas;
   private String Dura_Peliculas;
   private String Titulo_Pelicula;
   private Date Inic_Proyeccion;
   private Date Fin_Proyeccion;
   private String Clas_Peliculas;
       
   
   //Area de variables auxiliares
   
   //Area de constructores

    public TablPeliculasDTO() {
    }

    public TablPeliculasDTO(int id_Peliculas) {
        this.id_Peliculas = id_Peliculas;
    }

    public TablPeliculasDTO(int id_Peliculas, String Dura_Peliculas, String Titulo_Pelicula, Date Inic_Proyeccion, Date Fin_Proyeccion, String Clas_Peliculas) {
        this.id_Peliculas = id_Peliculas;
        this.Dura_Peliculas = Dura_Peliculas;
        this.Titulo_Pelicula = Titulo_Pelicula;
        this.Inic_Proyeccion = Inic_Proyeccion;
        this.Fin_Proyeccion = Fin_Proyeccion;
        this.Clas_Peliculas = Clas_Peliculas;
    }
      // area de getter
    public int getid_Peliculas(){
        return id_Peliculas;
    }

    public String getDura_Peliculas() {
        return Dura_Peliculas;
    }

    public String getTitulo_Pelicula() {
        return Titulo_Pelicula;
    }
    
    public Date getInic_Proyeccion() {
        return Inic_Proyeccion;
    }
    
    public Date getFin_Proyeccion() {
        return Fin_Proyeccion;
    }
    
    public String getClas_Peliculas() {
        return Clas_Peliculas;
        
    }

    
    
    public void setid_Peliculas(int id_Peliculas) {
        this.id_Peliculas = id_Peliculas;
    }

    public void setDura_Peliculas(String Dura_Peliculas) {
        this.Dura_Peliculas = Dura_Peliculas;
    }

    public void setTitulo_Pelicula(String Titulo_Pelicula) {
        this.Titulo_Pelicula = Titulo_Pelicula;
    }
    
    public void setInic_Proyeccion(Date Inic_Proyeccion) {
        this.Inic_Proyeccion = Inic_Proyeccion;
    }
    
    public void setFin_Proyeccion(Date Fin_Proyeccion){
        this.Fin_Proyeccion = Fin_Proyeccion;
    }
    
    public void setClas_Peliculas(String Clas_Peliculas) {
        this.Clas_Peliculas = Clas_Peliculas;
    }
    
    //Area de otros metodos
}
    
