/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modeloDTO;

/**
 *
 * @author Cesar.Gutierrez
 */
public class TablSalasDTO {
    // columnas de la base de datos de la tabla TABLSalas
    private int id_Salas;
    private int Nume_Salas;
    private int tabl_asientos_id_Asientos;
    private int tabl_peliculas_id_Peliculas;

    //area de variables auxiriales
    
    //area de constructores
    public TablSalasDTO() {
    }

    public TablSalasDTO(int id_Salas) {
        this.id_Salas = id_Salas;
    }

    public TablSalasDTO(int id_Salas, int Nume_Salas, int tabl_asientos_id_Asientos, int tabl_peliculas_id_Peliculas) {
        this.id_Salas = id_Salas;
        this.Nume_Salas = Nume_Salas;
        this.tabl_asientos_id_Asientos = tabl_asientos_id_Asientos;
        this.tabl_peliculas_id_Peliculas = tabl_peliculas_id_Peliculas;
    }

    //area de getter and setter
    public int getid_Salas() {
        return id_Salas;
    }

    public int getNume_Salas() {
        return Nume_Salas;
    }

    public int gettabl_asientos_id_Asientos() {
        return tabl_asientos_id_Asientos;
    }
    
    public int gettabl_peliculas_id_Peliculas() {
        return tabl_peliculas_id_Peliculas;
    }
    
    public void setId_Salas(int id_Salas) {
        this.id_Salas = id_Salas;
    }

    public void setNume_Salas(int Nume_Salas) {
        this.Nume_Salas = Nume_Salas;
    }
    
    public void settabl_asientos_id_Asientos(int tabl_asientos_id_Asientos) {
        this.tabl_asientos_id_Asientos = tabl_asientos_id_Asientos;
    }
    
    public void settabl_peliculas_id_Peliculas(int tabl_peliculas_id_Peliculas) {
        this.tabl_peliculas_id_Peliculas = tabl_peliculas_id_Peliculas;
    }

    //area de otros metodos
    
}