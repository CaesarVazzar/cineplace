/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import java.util.List;

/**
 *
 * @author vvs
 */
public interface interaccion <ObjetoDAO>{ // ObjetoDTO es un dato generico "Generic"
    // Solo se definen los métodos, no se escribe código
    // Ayuda para trabajo en equipo, para que todos utilicen los mismos métodos
    public boolean create(ObjetoDAO o);
    public boolean delete(Object llave);
    public boolean update(ObjetoDAO o);
    public ObjetoDAO read(Object llave);
    public List<ObjetoDAO> readAll();
    public List<ObjetoDAO> readMany(Object llave); 
    
}
