/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;
import com.mysql.jdbc.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author vvs
 */
public class conexion {
    public static conexion instance; // Sirve para aplicar el Singleton
    private Connection con;
    
    private conexion(){ // Privado para que no se pueda crear conexiones desde fuera
        try {
            Class.forName("com.mysql.jdbc.Driver"); 
            con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/db_cine?zeroDateTimeBehavior=convertToNull","root","");
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public synchronized static conexion verEstado(){
        if(instance == null){
            instance = new conexion();
        }
        return instance;
    }

    public Connection getCon() {
        return con;
    }
    
    public void cerrarConexion(){
        instance = null;
    }
}


