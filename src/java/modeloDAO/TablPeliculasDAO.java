/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modeloDAO;

import interfaces.interaccion;
import java.util.List;
import java.sql.PreparedStatement;
import conexion.conexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import modeloDTO.TablPeliculasDTO;

;

/**
 *
 * @author vvs
 */
public class TablPeliculasDAO implements interaccion<TablPeliculasDTO> {

    private static final String SQL_INSERT = "INSERT INTO db_cine.tabl_peliculas(id_Peliculas,Dura_Peliculas,Titulo_Pelicula,Inic_Proyeccion,Fin_Proyeccion,Clas_Peliculas) VALUES(?,?,?,?,?,?)";
    private static final String SQL_DELETE = "DELETE FROM  db_cine.tabl_peliculas WHERE ID_PELICULAS = ?";
    private static final String SQL_UPDATE = "UPDATE db_cine.tabl_peliculas SET Dura_Peliculas=?,Titulo_Pelicula=?,Inic_Proyeccion=?,Fin_Proyeccion=?,Clas_Peliculas=? WHERE id_Peliculas = ?";
    private static final String SQL_READ = " SELECT * FROM db_cine.tabl_peliculas WHERE ID_PELICULAS=? ";
    private static final String SQL_READ_ALL = " SELECT * FROM db_cine.tabl_peliculas";
    private static final String SQL_READ_MANY = " SELECT * FROM db_cine.tabl_peliculas WHERE ID_PELICULAS ";

    String error;
    private static final conexion con = conexion.verEstado(); //se aplica singlenton

    @Override
    public boolean create(TablPeliculasDTO o) {
        PreparedStatement ps;

        try {

            ps = con.getCon().prepareStatement(SQL_INSERT);
            ps.setInt(1, o.getid_Peliculas());
            ps.setString(2, o.getDura_Peliculas());
            ps.setString(3, o.getTitulo_Pelicula());
            ps.setDate(4, o.getInic_Proyeccion());
            ps.setDate(5, o.getFin_Proyeccion());
            ps.setString(6, o.getClas_Peliculas());

            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            error = ex.toString();
        } finally {
            con.cerrarConexion();
        }//End Finalli
        return false;
    }//End method

    @Override
    public boolean delete(Object llave) {
        PreparedStatement ps;
        try {
            ps = con.getCon().prepareStatement(SQL_DELETE);
            ps.setInt(1, Integer.parseInt(llave.toString()));
            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (NumberFormatException | SQLException ex) {
            error = ex.toString();
        } finally {
            con.cerrarConexion();
        }
        return false;
    }

    @Override
    public boolean update(TablPeliculasDTO o) {
        PreparedStatement ps;

        try {

            ps = con.getCon().prepareStatement(SQL_UPDATE);

            ps.setString(1, o.getDura_Peliculas());
            ps.setString(2, o.getTitulo_Pelicula());
            ps.setDate(3, o.getInic_Proyeccion());
            ps.setDate(4, o.getFin_Proyeccion());
            ps.setString(5, o.getClas_Peliculas());

            ps.setInt(6, o.getid_Peliculas());

            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            error = ex.toString();
        } finally {
            con.cerrarConexion();
        }//End Finalli
        return false;
    }//End methodo

    @Override
    public TablPeliculasDTO read(Object llave) {
        PreparedStatement ps;
        ResultSet rs;
        TablPeliculasDTO a = null;
        try {
            ps = con.getCon().prepareStatement(SQL_READ);
            ps.setInt(1, Integer.parseInt(llave.toString()));
            rs = ps.executeQuery();
            while (rs.next()) {
                a = new TablPeliculasDTO(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getDate(4), rs.getDate(5), rs.getString(6));
            }
            return a;
        } catch (NumberFormatException | SQLException ex) {
            error = ex.toString();
        } finally {
            con.cerrarConexion();
        }//End Finalli
        return a;
    }//End methodo

    @Override
    public List<TablPeliculasDTO> readAll() {
        PreparedStatement ps;
        ArrayList<TablPeliculasDTO> registros = new ArrayList();
        ResultSet rs;
        try {
            ps = con.getCon().prepareStatement(SQL_READ_ALL);
            rs = ps.executeQuery();
            while (rs.next()) {
                registros.add(new TablPeliculasDTO(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getDate(4), rs.getDate(5), rs.getString(6)));
            }
            return registros;
        } catch (NumberFormatException | SQLException ex) {
            error = ex.toString();
        }
        return registros;
    }

    public static int ObtieneSiguiente() {
        String SQL_READ_NEXT_ID = "SELECT IFNULL(MAX(id_Peliculas),0) + 1 From db_cine.tabl_peliculas";
        int nextID = 0;

        String error;
        PreparedStatement ps;
        ResultSet rs;
        try {
            ps = con.getCon().prepareStatement(SQL_READ_NEXT_ID);
            rs = ps.executeQuery();
            while (rs.next()) {
                nextID = rs.getInt(1);
            }
            return nextID;
        } catch (NumberFormatException | SQLException ex) {
            error = ex.toString();
        } finally {
            con.cerrarConexion();
        }//End Finally
        return nextID;
    }

    @Override
    public List<TablPeliculasDTO> readMany(Object llave) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
