/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modeloDAO;

import conexion.conexion;
import interfaces.interaccion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modeloDTO.TablSalasDTO;

/**
 *
 * @Author Cesar.Gutierrez
 */
public class TablSalasDAO implements interaccion<TablSalasDTO> {

    private static final String SQL_INSERT = "INSERT INTO db_cine.tabl_salas(id_Salas,Nume_Salas,tabl_asientos_id_Asientos, tabl_peliculas_id_Peliculas) VALUES(?,?,?,?)";
    private static final String SQL_DELETE = "DELETE FROM  db_cine.tabl_salas WHERE id_Salas = ?";
    private static final String SQL_UPDATE = "UPDATE db_cine.tabl_salas SET Nume_Salas=? WHERE id_Salas=?";
    private static final String SQL_READ = " SELECT * FROM db_cine.tabl_salas WHERE id_Salas = ?";
    private static final String SQL_READ_ALL = " SELECT * FROM db_cine.tabl_Salas";
    private static final String SQL_READ_MANY = " SELECT * FROM db_cine.tabl_salas WHERE id_Salas ";

    String error;
    private static final conexion con = conexion.verEstado(); //se aplica singlenton

    @Override //metodo del insert
    public boolean create(TablSalasDTO o) {
        PreparedStatement ps;

        try {

            ps = con.getCon().prepareStatement(SQL_INSERT);
            ps.setInt(1, o.getid_Salas());
            ps.setInt(2, o.getNume_Salas());
            ps.setInt(3, o.gettabl_asientos_id_Asientos());
            ps.setInt(4, o.gettabl_peliculas_id_Peliculas());
            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            error = ex.toString();
        } finally {
            con.cerrarConexion();
        }//End Finalli
        return false;
    }//End method

    @Override //metodo del delete
    public boolean delete(Object llave) {
        PreparedStatement ps;
        try {
            ps = con.getCon().prepareStatement(SQL_DELETE);
            ps.setInt(1, Integer.parseInt(llave.toString()));
            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (NumberFormatException | SQLException ex) {
            error = ex.toString();
        } finally {
            con.cerrarConexion();
        }
        return false;
    }

    @Override //metodo del update
    public boolean update(TablSalasDTO o) {
        PreparedStatement ps;

        try {

            ps = con.getCon().prepareStatement(SQL_UPDATE);

            ps.setInt(1, o.getNume_Salas());
            ps.setInt(2, o.gettabl_asientos_id_Asientos());
             ps.setInt(3, o.gettabl_peliculas_id_Peliculas());
            ps.setInt(4, o.getid_Salas());

            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            error = ex.toString();
        } finally {
            con.cerrarConexion();
        }//End Finalli
        return false;
    }//End methodo
    
    

    @Override
    public TablSalasDTO read(Object llave) {
        PreparedStatement ps;
        ResultSet rs;
        TablSalasDTO a = null;
        try {
            ps = con.getCon().prepareStatement(SQL_READ);
            ps.setInt(1, Integer.parseInt(llave.toString()));
            rs = ps.executeQuery();
            while (rs.next()) {
                a = new TablSalasDTO(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getInt(4));
            }
            return a;
        } catch (NumberFormatException | SQLException ex) {
            error = ex.toString();
        } finally {
            con.cerrarConexion();
        }//End Finalli
        return a;
    }//End methodo

    @Override
    public List<TablSalasDTO> readAll() {
        PreparedStatement ps;
        ArrayList<TablSalasDTO> registros = new ArrayList();
        ResultSet rs;
        try {
            ps = con.getCon().prepareStatement(SQL_READ_ALL);
            rs = ps.executeQuery();
            while (rs.next()) {
                registros.add(new TablSalasDTO(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getInt(4)));
            }
            return registros;
        } catch (NumberFormatException | SQLException ex) {
            error = ex.toString();
        }
        return registros;
    }


    public static int ObtieneSiguiente() {
        String SQL_READ_NEXT_ID = "SELECT IFNULL(MAX(id_Salas),0) + 1 From db_cine.tabl_salas";
        int nextID = 0;
        String error;
        PreparedStatement ps;
        ResultSet rs;
        try {
            ps = con.getCon().prepareStatement(SQL_READ_NEXT_ID);
            rs = ps.executeQuery();
            while (rs.next()) {
                nextID = rs.getInt(1);
            }
            return nextID;
        } catch (NumberFormatException | SQLException ex) {
            error = ex.toString();
        } finally {
            con.cerrarConexion();
        }//End Finally
        return nextID;
    }
    
    
    
    
    
    
    
    @Override
    public List<TablSalasDTO> readMany(Object llave) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
