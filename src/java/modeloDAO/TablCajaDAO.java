/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modeloDAO;

import interfaces.interaccion;
import java.util.List;
import modeloDTO.TablCajaDTO;
import java.sql.PreparedStatement;
import conexion.conexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Cesar.Gutierrez
 */
public class TablCajaDAO implements interaccion<TablCajaDTO> {

    private static final String SQL_INSERT = "INSERT INTO db_cine.tabl_caja(id_Transaccion,Tota_Caja,Form_Pago,Nume_Boletos,se_Modifico) VALUES(?,?,?,?,?)";
    private static final String SQL_DELETE = "DELETE FROM  db_cine.tabl_caja WHERE id_Transaccion = ?";
    private static final String SQL_UPDATE = "UPDATE db_cine.tabl_caja SET Tota_Caja=?, Form_Pago=?, Nume_Boletos=?, se_Modifico=? WHERE id_Transaccion=?";
    private static final String SQL_READ = " SELECT * FROM db_cine.tabl_caja WHERE id_Transaccion = ?";
    private static final String SQL_READ_ALL = " SELECT * FROM db_cine.tabl_caja";
    private static final String SQL_READ_MANY = " SELECT * FROM db_cine.tabl_caja WHERE id_Transaccion ";

    String error;
    private static final conexion con = conexion.verEstado(); //se aplica singlenton

    @Override
    public boolean create(TablCajaDTO o) {
        PreparedStatement ps;

        try {

            ps = con.getCon().prepareStatement(SQL_INSERT);
            ps.setInt(1, o.getid_Transaccion());
            ps.setInt(2, o.getTota_Caja());
            ps.setString(3, o.getForm_Pago());
            ps.setInt(4, o.getNume_Boletos());
            ps.setString(5, o.getse_Modifico());
            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            error = ex.toString();
        } finally {
            con.cerrarConexion();
        }//End Finalli
        return false;
    }//End method

    @Override
    public boolean delete(Object llave) {
        PreparedStatement ps;
        try {
            ps = con.getCon().prepareStatement(SQL_DELETE);
            ps.setInt(1, Integer.parseInt(llave.toString()));
            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (NumberFormatException | SQLException ex) {
            error = ex.toString();
        } finally {
            con.cerrarConexion();
        }
        return false;
    }

    @Override
    public boolean update(TablCajaDTO o) {
        PreparedStatement ps;

        try {

            ps = con.getCon().prepareStatement(SQL_UPDATE);

            ps.setInt(1, o.getTota_Caja());
            ps.setString(2, o.getForm_Pago());
            ps.setInt(3, o.getNume_Boletos());
            ps.setString(4, o.getse_Modifico());
            ps.setInt(5, o.getid_Transaccion());
            
            

            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            error = ex.toString();
        } finally {
            con.cerrarConexion();
        }//End Finally
        return false;
    }//End methodo

    @Override
    public TablCajaDTO read(Object llave) {
        PreparedStatement ps;
        ResultSet rs;
        TablCajaDTO a = null;
        try {
            ps = con.getCon().prepareStatement(SQL_READ);
            ps.setInt(1, Integer.parseInt(llave.toString()));
            rs = ps.executeQuery();
            while (rs.next()) {
                a = new TablCajaDTO(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getInt(4), rs.getString(5));
            }
            return a;
        } catch (NumberFormatException | SQLException ex) {
            error = ex.toString();
        } finally {
            con.cerrarConexion();
        }//End Finalli
        return a;
    }//End methodo

    @Override
    public List<TablCajaDTO> readAll() {
        PreparedStatement ps;
        ArrayList<TablCajaDTO> registros = new ArrayList();
        ResultSet rs;
        try {
            ps = con.getCon().prepareStatement(SQL_READ_ALL);
            rs = ps.executeQuery();
            while (rs.next()) {
                registros.add(new TablCajaDTO(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getInt(4), rs.getString(5)));
            }
            return registros;
        } catch (NumberFormatException | SQLException ex) {
            error = ex.toString();
        }
        return registros;
    }

    public static int ObtieneSiguiente() {
        String SQL_READ_NEXT_ID = "SELECT IFNULL(MAX(id_Transaccion),0) + 1 From db_cine.tabl_caja";
        int nextID = 0;

        String error;
        PreparedStatement ps;
        ResultSet rs;
        try {
            ps = con.getCon().prepareStatement(SQL_READ_NEXT_ID);
            rs = ps.executeQuery();
            while (rs.next()) {
                nextID = rs.getInt(1);
            }
            return nextID;
        } catch (NumberFormatException | SQLException ex) {
            error = ex.toString();
        } finally {
            con.cerrarConexion();
        }//End Finalli
        return nextID;
    }

    @Override
    public List<TablCajaDTO> readMany(Object llave) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}