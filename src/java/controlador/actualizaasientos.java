/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modeloDAO.TablAsientosDAO;
import modeloDTO.TablAsientosDTO;

/**
 *
 * @author Cesar.Gutierrez
 */
@WebServlet(name = "actualizaasientos", urlPatterns = {"/actualizaasientos.do"})
public class actualizaasientos extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        // try (PrintWriter out = response.getWriter()) {
        /* TODO output your page here. You may use following sample code. */
        //   out.println("<!DOCTYPE html>");
        //   out.println("<html>");
        //   out.println("<head>");
        //   out.println("<title>Servlet actualizapeliculas</title>");            
        //   out.println("</head>");
        //   out.println("<body>");
        //   out.println("<h1>Servlet actualizapeliculas at " + request.getContextPath() + "</h1>");
        //   out.println("</body>");
        //   out.println("</html>");
        //  }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        // Declaracion de clases
        TablAsientosDAO asientos = new TablAsientosDAO();
        TablAsientosDTO a;
        // Declaracion de ambiente
        HttpSession s = request.getSession();

        // Declaracion de variables de la base de datos
        int Id_Asientos = 0;
        int Num_Asiento = 0;
        String Letr_Asiento;
        String se_Cancela;
        //Declaracion de variables generales
        String error = "";
        String mensaje = "";
        boolean exito;

        //CUERPO DEL PROGRAMA
        //Recuperacion de variables de HTML
        Letr_Asiento = request.getParameter("Letr_Asiento");
        se_Cancela = request.getParameter("se_Cancela");
        
        
        
        
        // verificacion para que el int sea solo numero
        try {

            Num_Asiento = Integer.parseInt(request.getParameter("Num_Asiento"));

        } catch (Exception ex) {
            error = ex.toString();
            mensaje = "ERROR.... VALOR INVALIDO EN CAMPO NUMERICO...";
            s.setAttribute("mensaje", mensaje);
            response.sendRedirect("404.jsp");
        }

        
        
        Id_Asientos=TablAsientosDAO.ObtieneSiguiente(); //pedimos un ID
        if (Id_Asientos > 0) {
            a = new TablAsientosDTO(Id_Asientos, Num_Asiento, Letr_Asiento,se_Cancela);
            exito = asientos.create(a); //creamos el ID que acabamos de pedir
            exito = asientos.update(a); // lo actualizamos
            if (exito) {
                s.setAttribute("mensaje", mensaje);
            } else {
                mensaje = "Error al actualizar el asiento";
                s.setAttribute("mensaje", mensaje);
                response.sendRedirect("404.jsp");
            }
        } else {
            mensaje = "Error al obtener el ID del Asiento";
            s.setAttribute("mensaje", mensaje);

        }
        mensaje = "El Asiento se a Actualizado Correctamente";
        s.setAttribute("mensaje", mensaje);
        response.sendRedirect("exito.jsp");

    }

}
