/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modeloDAO.TablCajaDAO;
import modeloDTO.TablCajaDTO;

/**
 *
 * @author Cesar.Gutierrez
 */
@WebServlet(name = "actualizacaja", urlPatterns = {"/actualizacaja.do"})
public class actualizacaja extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        // try (PrintWriter out = response.getWriter()) {
        /* TODO output your page here. You may use following sample code. */
        //   out.println("<!DOCTYPE html>");
        //   out.println("<html>");
        //   out.println("<head>");
        //   out.println("<title>Servlet actualizapeliculas</title>");            
        //   out.println("</head>");
        //   out.println("<body>");
        //   out.println("<h1>Servlet actualizapeliculas at " + request.getContextPath() + "</h1>");
        //   out.println("</body>");
        //   out.println("</html>");
        //  }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        // Declaracion de clases
        TablCajaDAO caja = new TablCajaDAO();
        TablCajaDTO c;
        // Declaracion de ambiente
        HttpSession s = request.getSession();

        // Declaracion de variables de la base de datos
        int id_Transaccion = 0;
        int Tota_Caja = 0;
        String Form_Pago;
        int Nume_Boletos = 0;
        String se_Modifico;

        //Declaracion de variables generales
        String error = "";
        String mensaje = "";
        boolean exito;

        //CUERPO DEL PROGRAMA
        //Recuperacion de variables de HTML
        Form_Pago = request.getParameter("Form_Pago");
        se_Modifico = request.getParameter("se_Modifico");
        Nume_Boletos = Integer.parseInt(request.getParameter("Nume_Boletos"));

        
        
        //verificacion para que los int sean solo numeros
        try {

            Tota_Caja = Integer.parseInt(request.getParameter("Tota_Caja"));

        } catch (Exception ex) {
            error = ex.toString();

            mensaje = "ERROR.... VALOR INVALIDO EN CAMPO NUMERICO...";
            response.sendRedirect("menu.jsp");
        }

        id_Transaccion = TablCajaDAO.ObtieneSiguiente(); //pedimos un ID nuevo
        if (id_Transaccion > 0) {
            c = new TablCajaDTO(id_Transaccion, Tota_Caja, Form_Pago, Nume_Boletos, se_Modifico);
            exito = caja.create(c); //creamos un campo con el nuevo id
            exito = caja.update(c); //le damos update
            if (exito) {
                s.setAttribute("mensaje", mensaje);
            } else {
                mensaje = "Error al actualizar la caja";
                s.setAttribute("mensaje", mensaje);
                response.sendRedirect("404.jsp");
            }
        } else {
            mensaje = "Error al obtener el ID de la transaccion";
            s.setAttribute("mensaje", mensaje);
            response.sendRedirect("404.jsp");

        }
        mensaje = "La caja se a Actualizado Correctamente";
        s.setAttribute("mensaje", mensaje);
        response.sendRedirect("exito.jsp");

    }

}
