/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modeloDAO.TablCajaDAO;
import modeloDTO.TablCajaDTO;

/**
 *
 * @author Cesar Gutierrez
 */

@WebServlet(name = "buscacaja", urlPatterns = {"/buscacaja.do"})
public class buscacaja extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
       // try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
        //    out.println("<!DOCTYPE html>");
        //    out.println("<html>");
        //    out.println("<head>");
        //    out.println("<title>Servlet buscapeliculas</title>");            
        //    out.println("</head>");
        //    out.println("<body>");
        //    out.println("<h1>Servlet buscapeliculas at " + request.getContextPath() + "</h1>");
        //    out.println("</body>");
        //    out.println("</html>");
     //   }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
     // Declaracion de clases
        TablCajaDAO caja = new TablCajaDAO();
        TablCajaDTO c;
        // Declaracion de ambiente
        HttpSession s = request.getSession();
        // Declaracion de variables de base de datos
        int id_Transaccion=0;
        // Declaracion de variables generales
        String error;
        String mensaje;
        String x="";
        x=request.getParameter("id");
        // Cuerpo del Programa
        try{
            id_Transaccion=Integer.parseInt(request.getParameter("id"));
            
        }
        catch(Exception ex){
            error=ex.toString();
            mensaje="ERROR, El valor debe ser numerico...";
            s.setAttribute("mensaje", mensaje);
            response.sendRedirect("404.jsp");
        }
        
        try{
           c=caja.read(id_Transaccion);
            if(c.getid_Transaccion()<=0){
                mensaje="ATENCION, Registro no encontrado...";
                s.setAttribute("mensaje", mensaje);
                response.sendRedirect("404.jsp");
            }
            else{
                s.setAttribute("objetocaja", c);
                response.sendRedirect("consultacaja.jsp");
            }
        }
        catch(Exception ex){
            error=ex.toString();
            mensaje="Error al conectarse a la base de datos...";
            s.setAttribute("mensaje", mensaje);
            response.sendRedirect("404.jsp");
        }
    }
    
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
