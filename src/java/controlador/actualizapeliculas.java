/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modeloDAO.TablPeliculasDAO;
import modeloDTO.TablPeliculasDTO;

/**
 *
 * @author Cesar.Gutierrez
 */
@WebServlet(name = "actualizapeliculas", urlPatterns = {"/actualizapeliculas.do"})
public class actualizapeliculas extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        // try (PrintWriter out = response.getWriter()) {
        /* TODO output your page here. You may use following sample code. */
        //   out.println("<!DOCTYPE html>");
        //   out.println("<html>");
        //   out.println("<head>");
        //   out.println("<title>Servlet actualizapeliculas</title>");            
        //   out.println("</head>");
        //   out.println("<body>");
        //   out.println("<h1>Servlet actualizapeliculas at " + request.getContextPath() + "</h1>");
        //   out.println("</body>");
        //   out.println("</html>");
        //  }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        // Declaracion de clases
        TablPeliculasDAO peliculas = new TablPeliculasDAO();
        TablPeliculasDTO peli;
        // Declaracion de ambiente
        HttpSession s = request.getSession();

        // Declaracion de variables de la base de datos
        int id_Peliculas = 0;
        String Dura_Peliculas;
        String Titulo_Pelicula;
        String Clas_Peliculas;

        Date Inic_Proyeccion = new Date(2017, 01, 01);
        Date Fin_Proyeccion = new Date(2017, 01, 01);

        //Declaracion de variables generales
        String error = "";
        String mensaje = "";
        boolean exito;
        String fechaInicStr;
        String fechaFinStr;
        //CUERPO DEL PROGRAMA
        //Recuperacion de variables de HTML
        id_Peliculas = Integer.parseInt(request.getParameter("id_Peliculas"));
        Dura_Peliculas = request.getParameter("Dura_Peliculas");
        Titulo_Pelicula = request.getParameter("Titulo_Pelicula");
        Clas_Peliculas = request.getParameter("Clas_Peliculas");
        fechaInicStr = request.getParameter("Inic_Proyeccion");
        fechaFinStr = request.getParameter("Fin_Proyeccion");
        
        // Verificar Fecha de Inicio
        java.util.Date utilDate;
        java.sql.Date sqlDate;
        try {
            utilDate = new java.util.Date(fechaInicStr);
            Inic_Proyeccion = new java.sql.Date(utilDate.getTime());
        } catch (Exception ex) {
            error = ex.toString();
            mensaje = "ERROR... Valor invalido en campo fecha...";
            s.setAttribute("mensaje", mensaje);
            response.sendRedirect("404.jsp");
        }

        // Verificar fecha de termino.
        try {
            utilDate = new java.util.Date(fechaFinStr);
            Fin_Proyeccion = new java.sql.Date(utilDate.getTime());
        } catch (Exception ex) {
            error = ex.toString();
            mensaje = "ERROR... Valor invalido en campo fecha...";
            s.setAttribute("mensaje", mensaje);
            response.sendRedirect("404.jsp");
        }


        
        //cuerpo del update
        if (id_Peliculas > 0) {
            peli = new TablPeliculasDTO(id_Peliculas, Dura_Peliculas, Titulo_Pelicula, Inic_Proyeccion, Fin_Proyeccion, Clas_Peliculas);
            exito = peliculas.update(peli);
            if (exito) {
                s.setAttribute("mensaje", mensaje);
            } else {
                mensaje = "Error al actualizar la pelicula";
                s.setAttribute("mensaje", mensaje);
                response.sendRedirect("404.jsp");
            }
        } else {
            mensaje = "Error al obtener el ID de la pelicula";
            s.setAttribute("mensaje", mensaje);
            response.sendRedirect("404.jsp");

        }
        mensaje = "La Pelicula se a Actualizado Correctamente";
        s.setAttribute("mensaje", mensaje);
        response.sendRedirect("exito.jsp");

    }

}
