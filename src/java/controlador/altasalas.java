/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modeloDAO.TablSalasDAO;
import modeloDTO.TablSalasDTO;
import modeloDAO.TablAsientosDAO;
import modeloDTO.TablAsientosDTO;

/**
 *
 * @author Cesar.Gutierrez
 */
@WebServlet(name = "altasalas", urlPatterns = {"/altasalas.do"})
public class altasalas extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        // try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            /*
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AltaEmpleados</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AltaEmpleados at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
        */
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        
        // Declaracion de clases
        TablSalasDAO sala = new TablSalasDAO();
        TablSalasDTO sa;
        // Declaracion de ambiente
        HttpSession s = request.getSession();
        // Declaracion de variables de base de datos
        int id_Salas=0;
        int Nume_Salas=0;
        int tabl_asientos_id_Asientos=0;
        int tabl_peliculas_id_Peliculas=0;

        // Declaracion de variables generales
        String error="";
        String mensaje="Sin mensaje";
        boolean exito;
        // CUERPO DEL PROGRAMA
        
        
        //verificamos que los valores int sean solo numeros
        try{
           tabl_asientos_id_Asientos=Integer.parseInt(request.getParameter("tabl_asientos_id_Asientos"));
           tabl_peliculas_id_Peliculas=Integer.parseInt(request.getParameter("tabl_peliculas_id_Peliculas"));
           Nume_Salas=Integer.parseInt(request.getParameter("Nume_Salas"));
        }
        catch(Exception ex){
            error=ex.toString();
            
            mensaje="ERROR.... VALOR INVALIDO EN CAMPO NUMERICO...";
            response.sendRedirect("404.jsp");
        }
        
        
        //cuerpo del insert a la base de datos
        id_Salas=TablSalasDAO.ObtieneSiguiente();
        if(id_Salas>0){
            sa=new TablSalasDTO(id_Salas,Nume_Salas,tabl_asientos_id_Asientos,tabl_peliculas_id_Peliculas);
            exito=sala.create(sa);
            if(exito){
                mensaje="La sala se a dado de alta correctamente";
                s.setAttribute("mensaje", mensaje);
                response.sendRedirect("exito.jsp");
            }
            else{
                mensaje="Error al realizar la operacion. Verifique que los IDs ingresados existan en la base de datos";
                s.setAttribute("mensaje", mensaje);
                response.sendRedirect("404.jsp");
            }
        }
        else{
            mensaje="Error al obtener el ID de la Sala";
            s.setAttribute("mensaje", mensaje);
            response.sendRedirect("404.jsp");
        }
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
