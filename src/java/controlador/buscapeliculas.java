/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modeloDAO.TablPeliculasDAO;
import modeloDTO.TablPeliculasDTO;

/**
 *
 * @author Cesar Gutierrez
 */

@WebServlet(name = "buscapeliculas", urlPatterns = {"/buscapeliculas.do"})
public class buscapeliculas extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
       // try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
        //    out.println("<!DOCTYPE html>");
        //    out.println("<html>");
        //    out.println("<head>");
        //    out.println("<title>Servlet buscapeliculas</title>");            
        //    out.println("</head>");
        //    out.println("<body>");
        //    out.println("<h1>Servlet buscapeliculas at " + request.getContextPath() + "</h1>");
        //    out.println("</body>");
        //    out.println("</html>");
     //   }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
     // Declaracion de clases
        TablPeliculasDAO peliculas = new TablPeliculasDAO();
        TablPeliculasDTO e;
        // Declaracion de ambiente
        HttpSession s = request.getSession();
        // Declaracion de variables de base de datos
        int Id_Peliculas=0;
        // Declaracion de variables generales
        String error;
        String mensaje;
        String x="";
        x=request.getParameter("id");
        // Cuerpo del Programa
        try{
            Id_Peliculas=Integer.parseInt(request.getParameter("id"));
            
        }
        catch(Exception ex){
            error=ex.toString();
            mensaje="ERROR, El valor debe ser numerico...";
            s.setAttribute("mensaje", mensaje);
            response.sendRedirect("404.jsp");
        }
        
        try{
            e=peliculas.read(Id_Peliculas);
            if(e.getid_Peliculas()<=0){
                mensaje="ATENCION, Registro no encontrado...";
                s.setAttribute("mensaje", mensaje);
                response.sendRedirect("404.jsp");
            }
            else{
                s.setAttribute("objetopeliculas", e);
                response.sendRedirect("consultapelicula.jsp");
            }
        }
        catch(Exception ex){
            error=ex.toString();
            mensaje="Error al conectarse a la base de datos...";
            s.setAttribute("mensaje", mensaje);
            response.sendRedirect("404.jsp");
        }
    }
    
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
