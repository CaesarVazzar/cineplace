/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modeloDAO.TablCajaDAO;
import modeloDTO.TablCajaDTO;

/**
 *
 * @author Cesar.Gutierrez
 */
@WebServlet(name = "cajacontrolador", urlPatterns = {"/cajacontrolador.do"})
public class cajacontrolador extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        // try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            /*
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AltaEmpleados</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AltaEmpleados at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
        */
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        
        // Declaracion de clases
        TablCajaDAO caja = new TablCajaDAO();
        TablCajaDTO c;
        // Declaracion de ambiente
        HttpSession s = request.getSession();
        // Declaracion de variables de base de datos
        int id_Transaccion=0;
        int Tota_Caja=0;
        String Form_Pago;
        int Nume_Boletos=0;
        String se_Modifico;

        // Declaracion de variables generales
        String error="";
        String mensaje="Sin mensaje";
        boolean exito;
        // CUERPO DEL PROGRAMA
        
        Form_Pago=request.getParameter("Form_Pago");
        Nume_Boletos=Integer.parseInt(request.getParameter("Nume_Boletos"));
        se_Modifico=request.getParameter("se_Modifico");
        try {
            
        Tota_Caja=Integer.parseInt(request.getParameter("Tota_Caja"));
        
        }
        catch(Exception ex){
            error=ex.toString();
            mensaje="ERROR.... VALOR INVALIDO EN CAMPO NUMERICO...";
            s.setAttribute("mensaje", mensaje);
            response.sendRedirect("404.jsp");
        }
        
        
        
        id_Transaccion=TablCajaDAO.ObtieneSiguiente();
        if(id_Transaccion>0){
            c=new TablCajaDTO(id_Transaccion,Tota_Caja,Form_Pago,Nume_Boletos, se_Modifico);
            exito=caja.create(c);
            if(exito){
                mensaje="La transaccion se a guardado exitosamente";
                s.setAttribute("mensaje", mensaje);
                response.sendRedirect("exito.jsp");
            }
            else{
                mensaje="Error al realizar la venta";
                s.setAttribute("mensaje", mensaje);
                response.sendRedirect("404.jsp");
                
            }
        }
        else{
            mensaje="Error al obtener el ID de la transaccion";
            s.setAttribute("mensaje", mensaje);
            response.sendRedirect("404.jsp");
        }
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
