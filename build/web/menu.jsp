

<%@page import="modeloDTO.TablPeliculasDTO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Menu CinePlace</title>

        <!-- CSS de Bootstrap -->
        <link href="css/bootstrap.css" rel="stylesheet">

        <!-- CSS Personalizado -->
        <link href="css/stylish-portfolio.css" rel="stylesheet">

        <!-- Tipo de Fuente -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script>
            $(function () {
                $("#datepicker").datepicker();
            });

            $(function () {
                $("#datepicker2").datepicker();
            });
        </script>
    </head>
    <body>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">CinePlace</a>
                </div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="/CinePlace/index.html">Ir a Inicio </a></li>
                </ul>
            </div>
        </nav>


        <section id="services" class="services bg-primary">
            <div class="container">
                <div class="row text-center">
                    <div class="col-lg-10 col-lg-offset-1">
                        <h2>Menu De Altas</h2>
                        <hr class="small">
                        <div class="row">
                            <div class="col-md-6">
                                <div style="text-align:center;" class="portfolio-item">
                                    <!-- Empieza modal numero 1 -->
                                    <div>
                                        <a style="color:white;">Menu Salas</a>
                                    </div>
                                    <button type="button" class="portfolio-item" data-toggle="modal" data-target="#myModal1"><img style="width: 250px; height: 250px;" class="img-portfolio img-responsive" src="img/room.png"></button>
                                    <!-- Modal -->
                                    <div id="myModal1" class="modal fade" role="dialog">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Alta de Salas</h4>
                                                </div>
                                                <div class="modal-body" >
                                                    <form style="align-content:center;" id="altasalas" name="altasalas" method="post" action="altasalas.do">

                                                        <div class="form-group">
                                                            <div class="row">
                                                                <label class="col-md-4">Numero de Sala</label>
                                                                <div class="col-md-2">
                                                                    <input type="text" name="Nume_Salas" id="Nume_Salas"></a>
                                                                </div>
                                                            </div>
                                                        </div>   
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <label class="col-md-4">ID Asiento</label>
                                                                <div class="col-md-2">
                                                                    <input type="text" name="tabl_asientos_id_Asientos" id="tabl_asientos_id_Asientos"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <label class="col-md-4">ID Pelicula</label>
                                                                <div class="col-md-2">
                                                                    <input type="text" name="tabl_peliculas_id_Peliculas" id="tabl_peliculas_id_Peliculas"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <input class="btn btn-success" id="Aceptar" name="Aceptar" type="submit" value="ACEPTAR">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                                    <a href="jspsalas.jsp"><button  style="float: left;" type="button" class="btn btn-warning">Realizar una busqueda</button></a>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- Termina modal numero 1 -->

                            <!-- Empieza modal numero 2 -->
                            <div class="col-md-6">
                                <div class="portfolio-item">
                                    <div>
                                        <a style="color:white;">Menu Peliculas</a>
                                    </div>
                                    <button type="button" class="portfolio-item" data-toggle="modal" data-target="#myModal2"><img style="width: 250px; height: 250px;" class="img-portfolio img-responsive" src="img/movie.png"></button>
                                    <!-- Modal -->
                                    <div id="myModal2" class="modal fade" role="dialog">
                                        <div class="modal-dialog modal-lg">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Alta de Peliculas</h4>
                                                </div>
                                                <div class="modal-body" >
                                                    <form style="align-content:center;" id="altapeliculas" name="altapeliculas" method="post" action="altapeliculas.do">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <label class="col-md-4">Titulo de la Pelicula</label>
                                                                <div class="col-md-2">
                                                                    <input type="text" name="Titulo_Pelicula" id="Titulo_Pelicula"></a>
                                                                </div>
                                                            </div>
                                                        </div>  

                                                        <div class="form-group">
                                                            <div class="row">
                                                                <label class="col-md-4">Duracion de la Pelicula (H:MM)</label>
                                                                <div class="col-md-2">
                                                                    <input type="text" name="Dura_Peliculas" id="Dura_Peliculas"></a>
                                                                </div>
                                                            </div>
                                                        </div> 

                                                        <div class="form-group">
                                                            <div class="row">
                                                                <label class="col-md-4">Clasificacion de la Pelicula</label>
                                                                <div class="col-md-2">
                                                                    <input type="text" name="Clas_Peliculas" id="Clas_Peliculas"></a>
                                                                </div>
                                                            </div>
                                                        </div> 

                                                        <div class="form-group">
                                                            <div class="row">
                                                                <label class="col-md-4">Fecha que Inicia la Proyeccion</label>
                                                                <div class="col-md-2">
                                                                    <input type="text" name="Inic_Proyeccion" id="datepicker"></a>
                                                                </div>
                                                            </div>
                                                        </div> 

                                                        <div class="form-group">
                                                            <div class="row">
                                                                <label class="col-md-4">Fecha que Termina la Proyeccion</label>
                                                                <div class="col-md-2">
                                                                    <input type="text" name="Fin_Proyeccion" id="datepicker2"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <input class="btn btn-success" id="Aceptar" name="Aceptar" type="submit" value="ACEPTAR">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                                    <a href="jsppeliculas.jsp"><button  style="float: left;" type="button" class="btn btn-warning">Realizar una busqueda</button></a>
                                                </div>   
                                                </form>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Termina modal numero 2 -->

                            <!-- Empieza modal numero 3 -->
                            <div class="col-md-6">
                                <div class="portfolio-item">
                                    <div>
                                        <a style="color:white;">Menu Asientos</a>
                                    </div>
                                    <button type="button" class="portfolio-item" data-toggle="modal" data-target="#myModal3"><img style="width: 250px; height: 250px;" class="img-portfolio img-responsive" src="img/seat.png"></button>
                                    <!-- Modal -->
                                    <div id="myModal3" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Dar de Alta un Asiento</h4>
                                                </div>
                                                <div class="modal-body" >
                                                    <form style="align-content:center;" id="Asientos" name="asignasientos" method="post" action="asignasientos.do">

                                                        <div class="form-group">
                                                            <div class="row">
                                                                <label class="col-md-4">Numero de Asiento</label>
                                                                <div class="col-md-2">
                                                                    <select name="Numero_Asiento">
                                                                            <option value="1" name="Numero_Asiento">1</option> 
                                                                            <option value="2" name="Numero_Asiento">2</option>
                                                                            <option value="3" name="Numero_Asiento">3</option>
                                                                            <option value="4" name="Numero_Asiento">4</option>
                                                                            <option value="5" name="Numero_Asiento">5</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div> 
                                                        
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <label class="col-md-4">Fila del Asiento</label>
                                                                <div class="col-md-2">
                                                                    <select name="Letr_Asiento">
                                                                            <option value="A" name="Letr_Asiento">A</option> 
                                                                            <option value="B" name="Letr_Asiento">B</option>
                                                                            <option value="C" name="Letr_Asiento">C</option>
                                                                            <option value="D" name="Letr_Asiento">D</option>
                                                                            <option value="E" name="Letr_Asiento">E</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <input class="btn btn-success" id="Aceptar" name="Aceptar" type="submit" value="ACEPTAR">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                                    <a href="jspasientos.jsp"><button  style="float: left;" type="button" class="btn btn-warning">Realizar una busqueda</button></a>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Termina modal numero 3 -->

                            <!-- Empieza modal numero 4 -->
                            <div class="col-md-6">
                                <div class="portfolio-item">
                                    <div>
                                        <a style="color:white;">Menu Ventas</a>
                                    </div>
                                    <button type="button" class="portfolio-item" data-toggle="modal" data-target="#myModal4"><img style="width: 250px; height: 250px;" class="img-portfolio img-responsive" src="img/register.png"></button>
                                    <!-- Modal -->
                                    <div id="myModal4" class="modal fade" role="dialog" >
                                        <div class="modal-dialog">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Dar de Alta una Venta</h4>
                                                    </div>
                                                    <div class="modal-body" >
                                                        <form style="align-content:center;" id="cajacontrolador" name="cajacontrolador" method="post" action="cajacontrolador.do">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-md-4">Forma de Pago</label>
                                                                    <div class="col-md-2">
                                                                        <select name="Form_Pago">
                                                                            <option selected value="0"> Elige una opción </option>
                                                                            <option value="Efectivo" name="Form_Pago">Efectivo</option> 
                                                                            <option value="Tarjeta" name="Form_Pago">Tarjeta (Solo visa)</option> 
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>  
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-md-4">Numero de Boletos</label>
                                                                    <div class="col-md-2">
                                                                        <input type="text" name="Nume_Boletos" id="Nume_Boletos"></a>
                                                                    </div>
                                                                </div>
                                                            </div> 
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-md-4">Total de la Venta</label>
                                                                    <div class="col-md-2">
                                                                        <input type="text" name="Tota_Caja" id="Tota_Caja"></a>
                                                                    </div>
                                                                </div>
                                                            </div>  
                                                            
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-md-2">
                                                                        <input hidden="true" value="no" type="text" name="se_Modifico" id="se_Modifico"></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <input class="btn btn-success" id="Aceptar" name="Aceptar" type="submit" value="ACEPTAR">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                                        <a href="jsptransaccion.jsp"><button style="float: left;" type="button" class="btn btn-warning">Realizar una busqueda</button></a>
                                                    </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.col-lg-10 -->
                    </div>
                    <!-- /.row -->
                </div>
            </div>
            <!-- /.container -->
        </section>



<!--        <section id="about" class="about">
            <div class="container">
                <div class="row text-center">
                    <div class="col-lg-10 col-lg-offset-1">
                        <h2>Menu de Bajas</h2>
                         Empieza modal numero 5 
                        <div class="col-md-6">
                            <div class="portfolio-item">
                                <div>
                                    <p style="color:black;">Baja Salas</p>
                                </div>
                                <button type="button" class="portfolio-item" data-toggle="modal" data-target="#myModal5"><img style="width: 250px; height: 250px;" class="img-portfolio img-responsive" src="img/deleteroom.png"></button>
                                 Modal 
                                <div id="myModal5" class="modal fade" role="dialog">
                                    <div class="modal-dialog modal-lg">
                                         Modal content
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Baja Salas</h4>
                                            </div>
                                            <div class="modal-body">
                                                <form ID="AltaEmpleados" name="empleados" method="post" action="AltaEmpleados.do">
                                                    <div class="form-group">
                                                        <label class="col-lg-3 control-label">Titulo Pelicula</label>
                                                        <div class="col-lg-8">
                                                            <input style="display: block;" type="text" name="NOMB_EMPLEADOS" id="NOMB_EMPLEADOS"   ></a>
                                                            <br />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-lg-3 control-label" style="padding-top:5px;">Duracion de la pelicula</label>
                                                        <div class="col-lg-8">
                                                            <input style="display: block;" type="text" name="PATE_EMPLEADOS" id="PATE_EMPLEADOS" >
                                                            <br />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-lg-3 control-label" style="padding-top:5px;">Horario de la pelicula (foraneo)</label>
                                                        <div class="col-lg-8">
                                                            <input style="display: block;" type="text" name="MATE_EMPLEADOS" id="MATE_EMPLEADOS" >
                                                            <br />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-lg-3 control-label" style="padding-top:5px;">Clasificacion pelicula</label>
                                                        <div class="col-lg-8">
                                                            <input style="display: block;" type="text" name="DIRE_EMPLEADOS" id="DIRE_EMPLEADOS">
                                                            <br />
                                                        </div>
                                                    </div>

                                            </div>

                                            <div class="modal-footer">
                                                <input class="btn btn-success" id="Aceptar" name="Enviar" type="submit" value="ACEPTAR">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                            </div>
                                            </form>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div> Termina modal numero 5 

                         Empieza modal numero 6 
                        <div class="col-md-6">
                            <div class="portfolio-item">
                                <div>
                                    <p style="color:black;">Baja Peliculas</p>
                                </div>
                                <button type="button" class="portfolio-item" data-toggle="modal" data-target="#myModal6"><img style="width: 250px; height: 250px;" class="img-portfolio img-responsive" src="img/deletemovie.png"></button>
                                 Modal 
                                <div id="myModal6" class="modal fade" role="dialog">
                                    <div class="modal-dialog modal-lg">
                                         Modal content
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">

                                                </h4>
                                            </div>
                                            <div class="modal-body">
                                                <form ID="AltaEmpleados" name="empleados" method="post" action="AltaEmpleados.do">
                                                    <div class="form-group">
                                                        <label class="col-lg-3 control-label">Titulo Pelicula</label>
                                                        <div class="col-lg-8">
                                                            <input style="display: block;" type="text" name="NOMB_EMPLEADOS" id="NOMB_EMPLEADOS"   ></a>
                                                            <br />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-lg-3 control-label" style="padding-top:5px;">Duracion de la pelicula</label>
                                                        <div class="col-lg-8">
                                                            <input style="display: block;" type="text" name="PATE_EMPLEADOS" id="PATE_EMPLEADOS" >
                                                            <br />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-lg-3 control-label" style="padding-top:5px;">Horario de la pelicula (foraneo)</label>
                                                        <div class="col-lg-8">
                                                            <input style="display: block;" type="text" name="MATE_EMPLEADOS" id="MATE_EMPLEADOS" >
                                                            <br />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-lg-3 control-label" style="padding-top:5px;">Clasificacion pelicula</label>
                                                        <div class="col-lg-8">
                                                            <input style="display: block;" type="text" name="DIRE_EMPLEADOS" id="DIRE_EMPLEADOS">
                                                            <br />
                                                        </div>
                                                    </div>

                                            </div>

                                            <div class="modal-footer">
                                                <input class="btn btn-success" id="Aceptar" name="Enviar" type="submit" value="ACEPTAR">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                            </div>
                                            </form>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div> Termina modal numero 6 

                         Empieza modal numero 7 
                        <div class="col-md-6">
                            <div class="portfolio-item">
                                <div>
                                    <p style="color:black;">Desocupar un asiento</p>
                                </div>
                                <button type="button" class="portfolio-item" data-toggle="modal" data-target="#myModal7"><img style="width: 250px; height: 250px;" class="img-portfolio img-responsive" src="img/deleteseat.png"></button>
                                 Modal 
                                <div id="myModal7" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                         Modal content
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Desocupar un asiento</h4>
                                            </div>
                                            <div class="modal-body">

                                            </div>
                                            <div class="modal-footer">
                                                <input class="btn btn-success" id="Aceptar" name="Enviar" type="submit" value="ACEPTAR">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                         Termina modal numero 7 

                         Empieza modal numero 8 
                        <div class="col-md-6">
                            <div class="portfolio-item">
                                <div>
                                    <a style="color:black;">Cancelar una venta</a>
                                </div>
                                <button type="button" class="portfolio-item" data-toggle="modal" data-target="#myModal8"><img style="width: 250px; height: 250px;" class="img-portfolio img-responsive" src="img/deleteregister.png"></button>


                                 Modal 
                                <div id="myModal8" class="modal fade" role="dialog" >
                                    <div class="modal-dialog">

                                         Modal content
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Cancelar una venta</h4>
                                            </div>
                                            <div class="modal-body">



                                            </div>
                                            <div class="modal-footer">
                                                <input class="btn btn-success" id="Aceptar" name="Enviar" type="submit" value="ACEPTAR">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                            </div>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>  Termina modal numero 8 
                    </div>
                </div>
                 /.row 
            </div>
             /.container 
        </section>-->


        <!-- Script de jQuery -->
        <script src="js/jquery.js"></script>

        <!-- JavaScript de Bootstrap -->
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>
