<%-- 
    Document   : 404
    Created on : 30/05/2017, 12:42:24 PM
    Author     : Cesar.Gutierrez
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>404</title>

        <!-- CSS de Bootstrap -->
        <link href="css/bootstrap.css" rel="stylesheet">

        <!-- CSS Personalizado -->
        <link href="css/stylish-portfolio.css" rel="stylesheet">

        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    </head>
    <body>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="/CinePlace/index.html">CinePlace</a>
                </div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="/CinePlace/menu.jsp">Ir a Menu </a></li>
                </ul>
            </div>
        </nav>

        <h1 style="text-align: center; margin-top:300px;"><%
            String msg = "";
            String error = "";
            try {
                msg = session.getAttribute("mensaje").toString();
            } catch (Exception ex) {
                error = ex.toString();
            }
            %>
            <p><%= msg%></p>
        </h1>
    </body>
</html>
