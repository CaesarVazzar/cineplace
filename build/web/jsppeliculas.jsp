<%-- 
    Document   : buscaEmpleadoActualizar
    Created on : 2/05/2017, 07:43:47 PM
    Author     : vvs
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>BUSQUEDAS</title>

        <!-- CSS de Bootstrap -->
        <link href="css/bootstrap.css" rel="stylesheet">

        <!-- CSS Personalizado -->
        <link href="css/stylish-portfolio.css" rel="stylesheet">

        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

    </head>

    <body>

        <style>#search {

            }
            #search input[type="text"] {
                background: url(search-white.png) no-repeat 10px 6px #fcfcfc;
                border: 1px solid #d1d1d1;
                font: bold 12px Arial,Helvetica,Sans-serif;
                color: #bebebe;
                width: 250px;
                padding: 6px 15px 6px 15px;
                -webkit-border-radius: 20px;
                -moz-border-radius: 20px;
                border-radius: 20px;
                text-shadow: 0 2px 3px rgba(0, 0, 0, 0.1);
                -webkit-box-shadow: 0 1px 3px rgba(0, 0, 0, 0.15) inset;
                -moz-box-shadow: 0 1px 3px rgba(0, 0, 0, 0.15) inset;
                box-shadow: 0 1px 3px rgba(0, 0, 0, 0.15) inset;
                -webkit-transition: all 0.7s ease 0s;
                -moz-transition: all 0.7s ease 0s;
                -o-transition: all 0.7s ease 0s;
                transition: all 0.7s ease 0s;
            }

            #search input[type="text"]:focus {
                width: 350px;
            }
        </style>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="/CinePlace/index.html">CinePlace</a>
                </div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="/CinePlace/menu.jsp">Ir a Menu </a></li>
                </ul>
            </div>
        </nav>


        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <form method="post" action="buscapeliculas.do" name="form1" id="search">
                        <input style="display: inline-block;" type="text" size="120" name="id" placeholder="Buscar una pelicula por ID..." />
                        <div style="margin-top: 120px; margin-left:170px;">
                            <input class="btn btn-success" id="aceptar" name="aceptar" type="submit" value="ACEPTAR">
                        </div>

                </div>
            </div>
        </div>
    </form>
</body>
</html>
