<%-- 
    Document   : consultapelicula
    Created on : 21/05/2017, 11:04:19 AM
    Author     : Cesar Gutierrez
--%>

<%@page import="modeloDTO.TablPeliculasDTO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href="css/bootstrap.css" rel="stylesheet">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Resultados de Busqueda</title>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script type="text/javascript">

            function yesnoCheck() {
                if (document.getElementById('yesCheck').checked) {
                    document.getElementById('ifYes').style.display = 'block';
                } else
                    document.getElementById('ifYes').style.display = 'none';

            }
            $(function () {
                $("#datepicker").datepicker();
            });

            $(function () {
                $("#datepicker2").datepicker();
            });
        </script>


    </head>
    <body>
        <style>
            @import "http://fonts.googleapis.com/css?family=Roboto:300,400,500,700";

            .container { margin-top: 20px; }
            .mb20 { margin-bottom: 20px; } 

            hgroup { padding-left: 15px; border-bottom: 1px solid #ccc; }
            hgroup h1 { font: 500 normal 1.625em "Roboto",Arial,Verdana,sans-serif; color: #2a3644; margin-top: 0; line-height: 1.15; }
            hgroup h2.lead { font: normal normal 1.125em "Roboto",Arial,Verdana,sans-serif; color: #2a3644; margin: 0; padding-bottom: 10px; }

            .search-result .thumbnail { border-radius: 0 !important; }
            .search-result:first-child { margin-top: 0 !important; }
            .search-result { margin-top: 20px; }
            .search-result .col-md-2 { border-right: 1px dotted #ccc; min-height: 140px; }
            .search-result ul { padding-left: 0 !important; list-style: none;  }
            .search-result ul li { font: 400 normal .85em "Roboto",Arial,Verdana,sans-serif;  line-height: 30px; }
            .search-result ul li i { padding-right: 5px; }
            .search-result .col-md-7 { position: relative; }
            .search-result h3 { font: 500 normal 1.375em "Roboto",Arial,Verdana,sans-serif; margin-top: 0 !important; margin-bottom: 10px !important; }
            .search-result h3 > a, .search-result i { color: #248dc1 !important; }
            .search-result p { font: normal normal 1.125em "Roboto",Arial,Verdana,sans-serif; } 
            .search-result span.plus { position: absolute; right: 0; top: 126px; }
            .search-result span.plus a { background-color: #248dc1; padding: 5px 5px 3px 5px; }
            .search-result span.plus a:hover { background-color: #414141; }
            .search-result span.plus a i { color: #fff !important; }
            .search-result span.border { display: block; width: 97%; margin: 0 15px; border-bottom: 1px dotted #ccc; }
        </style>

        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="/CinePlace/index.html">CinePlace</a>
                </div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="/CinePlace/menu.jsp">Ir a Menu </a></li>
                </ul>
            </div>
        </nav>
        <div class="container">
            <div class="container">
                <hgroup class="mb20">
                    <h3>Numero de  ID</h3>
                    <h2 style="margin-left: 60px;" class="lead"><strong class="text-danger">
                            <%
                                TablPeliculasDTO e = (TablPeliculasDTO) session.getAttribute("objetopeliculas");
                                out.println("<p>" + e.getid_Peliculas() + "</p>");
                                out.println(" <input type=\"text\" id=\"id\" name=\"id_Peliculas\" value=\" " + e.getid_Peliculas() + " \" hidden=\"true\"> ");
                            %>
                        </strong> </h2>							
                </hgroup>

                <section class="col-xs-12 col-sm-6 col-md-12">
                    <article class="search-result row">
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <a href="#" title="Lorem ipsum" class="thumbnail"><img src="https://images-na.ssl-images-amazon.com/images/M/MV5BMTczNDk4NTQ0OV5BMl5BanBnXkFtZTcwNDAxMDgxNw@@._V1_UX182_CR0,0,182,268_AL_.jpg" alt="tGwTDT" /></a>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-2">
                            <ul class="meta-search">
                                <li><i class="glyphicon glyphicon-calendar"></i> <span>Inicio de Proyeccion:<br />&emsp;&emsp;&emsp;<%out.println(e.getInic_Proyeccion());%></span></li>
                                <li><i class="glyphicon glyphicon-calendar"></i> <span>Fin de Proyeccion<br />&emsp;&emsp;&emsp;<%out.println(e.getFin_Proyeccion());%></span></li>
                                <li><i class="glyphicon glyphicon-time"></i><span>Duracion (H:MM):<br />&emsp;&emsp;&emsp;&emsp;<%out.println(e.getDura_Peliculas());%></span></li>
                                <li><i class="glyphicon glyphicon-tags"></i> <span>Casificacion<br />&emsp;&emsp;&emsp;&emsp;<%out.println(e.getClas_Peliculas());%></span></li>
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-7 excerpet">
                            <h3><p href="#" title="">
                                    <%
                                        out.println("<p>" + e.getTitulo_Pelicula() + "</p>");
                                    %>

                                </p></h3>
                            <h4 class="text-primary">

                            </h4>
                            <strong>Pequeña Descripcion (podria hacerse un query a la base de datos para mostrar la descripcion)</strong>
                            <p></p>
                        </div>
                        <span class="clearfix borda"></span>
                    </article>
                </section>                     
            </div>
        </div>


        <div style="padding-top:75px;"class="container">
            <form id="actualizapeliculas" name="actualizapeliculas" action="actualizapeliculas.do" method="post">
                <p>¿Desea hacer cambios a esta busqueda?</p>
                Si <input type="radio" onclick="javascript:yesnoCheck();" name="yesno" id="yesCheck"> No <input type="radio" onclick="javascript:yesnoCheck();" name="yesno" id="noCheck"><br>
                <div id="ifYes" style="display:none"><table>

                        <tr>
                            <td></td>
                            <td>
                                <%                                out.println(" <input type=\"text\" id=\"id\" name=\"id_Peliculas\" value=\"" + e.getid_Peliculas() + "\" hidden=\"true\"> ");
                                %>
                            </td>
                        </tr>
                        <tr>


                            <td>Titulo de la Pelicula</td>
                            <td>
                                <%                                out.println("<input type=\"text\" id=\"titulo\" name=\"Titulo_Pelicula\" value=\"" + e.getTitulo_Pelicula() + "\"> ");
                                %>
                            </td>

                            <td></td>
                            <td>Duracion de la Pelicula</td>
                            <td>
                                <%                                out.println("<input type=\"text\" id=\"duracion\" name=\"Dura_Peliculas\" value=\"" + e.getDura_Peliculas() + "\"> ");

                                %>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Clasificacion de la Pelicula:</td>
                            <td>
                                <%                                out.println("<input type=\"text\" id=\"Clas_Peliculas\" name=\"Clas_Peliculas\" value=\"" + e.getClas_Peliculas() + "\"> ");
                                %>
                            </td>
                            <td></td>
                            <td>Fecha que inicia la proyeccion</td>
                            <td>
                                <%
                                    out.println("<input type=\"text\" id=\"datepicker\" name=\"Inic_Proyeccion\" value=\"" + e.getInic_Proyeccion() + "\"> ");
                                %>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Fecha que termina la proyeccion</td>
                            <td>
                                <%
                                    out.println("<input type=\"text\" id=\"datepicker2\" name=\"Fin_Proyeccion\" value=\"" + e.getFin_Proyeccion() + "\"> ");
                                %>
                            </td>
                            <td></td>
                        </tr>
                        <tr></tr>
                        <tr></tr>
                        <tr></tr>
                        <tr></tr>
                        <tr>
                            <td>
                                <input id="aceptar" name="aceptar" type="submit" value="ENVIAR">
                            </td>
                        </tr>

                        <tr></tr>
                        <tr></tr>
                        <tr></tr>
                        <tr>
                        </tr>
                    </table>
                </div>
            </form>
        </div>

    </body>
</html>
