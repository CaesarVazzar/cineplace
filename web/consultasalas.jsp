<%-- 
    Document   : consultapelicula
    Created on : 21/05/2017, 11:04:19 AM
    Author     : Cesar Gutierrez
--%>

<%@page import="modeloDTO.TablSalasDTO"%>
<%@page import="modeloDTO.TablAsientosDTO"%>
<%@page import="modeloDTO.TablPeliculasDTO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href="css/bootstrap.css" rel="stylesheet">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Resultados de Busqueda</title>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/stylish-portfolio.css" rel="stylesheet">
        <script type="text/javascript">




            function yesnoCheck() {
                if (document.getElementById('yesCheck').checked) {
                    document.getElementById('ifYes').style.display = 'block';
                } else
                    document.getElementById('ifYes').style.display = 'none';
            }

            document.getElementById('no_modificar').readOnly = true;

        </script>


    </head>
    <body>
        <style>
            @import "http://fonts.googleapis.com/css?family=Roboto:300,400,500,700";

            .container { margin-top: 20px; }
            .mb20 { margin-bottom: 20px; } 

            hgroup { padding-left: 15px; border-bottom: 1px solid #ccc; }
            hgroup h1 { font: 500 normal 1.625em "Roboto",Arial,Verdana,sans-serif; color: #2a3644; margin-top: 0; line-height: 1.15; }
            hgroup h2.lead { font: normal normal 1.125em "Roboto",Arial,Verdana,sans-serif; color: #2a3644; margin: 0; padding-bottom: 10px; }

            .search-result .thumbnail { border-radius: 0 !important; }
            .search-result:first-child { margin-top: 0 !important; }
            .search-result { margin-top: 20px; }
            .search-result .col-md-2 { border-right: 1px dotted #ccc; min-height: 140px; }
            .search-result ul { padding-left: 0 !important; list-style: none;  }
            .search-result ul li { font: 400 normal .85em "Roboto",Arial,Verdana,sans-serif;  line-height: 30px; }
            .search-result ul li i { padding-right: 5px; }
            .search-result .col-md-7 { position: relative; }
            .search-result h3 { font: 500 normal 1.375em "Roboto",Arial,Verdana,sans-serif; margin-top: 0 !important; margin-bottom: 10px !important; }
            .search-result h3 > a, .search-result i { color: #248dc1 !important; }
            .search-result p { font: normal normal 1.125em "Roboto",Arial,Verdana,sans-serif; } 
            .search-result span.plus { position: absolute; right: 0; top: 126px; }
            .search-result span.plus a { background-color: #248dc1; padding: 5px 5px 3px 5px; }
            .search-result span.plus a:hover { background-color: #414141; }
            .search-result span.plus a i { color: #fff !important; }
            .search-result span.border { display: block; width: 97%; margin: 0 15px; border-bottom: 1px dotted #ccc; }
        </style>

        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="/CinePlace/index.html">CinePlace</a>
                </div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="/CinePlace/menu.jsp">Ir a Menu </a></li>
                </ul>
            </div>
        </nav>
        <div class="container">
            <div class="container">
                <hgroup class="mb20">
                    <h3 style="text-align:center;">Numero de  Sala</h3>
                    <h2 style="text-align:center;" class="lead"><strong class="text-danger">
                            <%
                                TablSalasDTO sa = (TablSalasDTO) session.getAttribute("objetosalas");
                                out.println("<p>" + sa.getNume_Salas() + "</p>");
                                
                            %>
                        </strong> </h2>							
                </hgroup>

                <section class="col-xs-12 col-sm-6 col-md-12">
                    <article class="search-result row">
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <a class="thumbnail"><img src="https://ak3.picdn.net/shutterstock/videos/9097148/thumb/10.jpg?i10c=img.resize(height:160)" alt="tGwTDT" /></a>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-2">
                            <ul class="meta-search">
                                <li><i class="glyphicons glyphicons-ticket"></i> <span>Numero de esta Sala<br />&emsp;&emsp;&emsp;&emsp;&emsp;<%out.println(sa.getNume_Salas());%></span></li>



                                <li><i class="glyphicons glyphicons-credit-card"></i><span>Esta sala esta asignada al siguiente ID de asiento<br />&emsp;&emsp;&emsp;&emsp;<%out.println(sa.gettabl_asientos_id_Asientos());%></span></li>
                            </ul>
                            </form>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-7 excerpet">
                            <h4 class="text-primary">

                            </h4>
                            
                            
                            
                            
                            
                            <!-- Empieza modal  -->
                            <div class="col-md-6">
                                <div class="portfolio-item">
                                    <div>
                                        <a style="color:white;">Dar de alta un asiento</a>
                                    </div>
                                    <button type="button" class="portfolio-item" data-toggle="modal" data-target="#myModal">Verificar a que ID de Pelicula esta asignada esta sala</button>
                                    <!-- Modal -->
                                    <div id="myModal" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Busqueda</h4>
                                                </div>
                                                <div class="modal-body" >
                                                <form style="align-content:center;" id="altasalas" name="altasalas" method="post" action="buscapeliculas.do">

                                                    <div class="form-group">
                                                        <div class="row">
                                                            <label class="col-md-4">Esta sala esta asignada al siguiente ID de Pelicula</label>
                                                            <div class="col-md-2">
                                                                <input value="<%out.println(sa.gettabl_peliculas_id_Peliculas());%>" type="text" readonly="readonly" name="id" id="no_modificar">
                                                            </div>
                                                        </div>
                                                    </div>   
                                            </div>
                                                <div class="modal-footer">
                                                    <input class="btn btn-success" id="Aceptar" name="Aceptar" type="submit" value="ACEPTAR">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                                    <a href="jspasientos.jsp"><button  style="float: left;" type="button" class="btn btn-warning">Realizar una busqueda</button></a>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Termina modal  -->
                            
                            
                            <!-- Empieza modal  -->
                            <div class="col-md-6">
                                <div class="portfolio-item">
                                    <div>
                                        <a style="color:white;">Busqueda de Asientos</a>
                                    </div>
                                    <button type="button" class="portfolio-item" data-toggle="modal" data-target="#myModal2">Verificar a que ID de Asientos esta asignada</button>
                                    <!-- Modal -->
                                    <div id="myModal2" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Busqueda</h4>
                                                </div>
                                                <div class="modal-body" >
                                                <form style="align-content:center;" id="altasalas" name="altasalas" method="post" action="buscatodosasientos.do">

                                                    <div class="form-group">
                                                        <div class="row">
                                                            <label class="col-md-4">Esta sala esta asignada al siguiente ID de Pelicula</label>
                                                            <div class="col-md-2">
                                                                <input value="<%out.println(sa.gettabl_asientos_id_Asientos());%>" type="text" readonly="readonly" name="idddddd" id="no_modificar">
                                                            </div>
                                                        </div>
                                                    </div>   
                                            </div>
                                                <div class="modal-footer">
                                                    <input class="btn btn-success" id="Aceptar" name="Aceptar" type="submit" value="ACEPTAR">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                                    <a href="jspasientos.jsp"><button  style="float: left;" type="button" class="btn btn-warning">Realizar una busqueda</button></a>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Termina modal  -->
                            
                            
                            
                            
        
                        </div> 
                        </div>
                        <span class="clearfix borda"></span>
                    </article>
                </section>                     
            </div>
        </div>


        <div style="padding-top:75px;"class="container">
            <form id="actualizacaja" name="actualizacaja" action="actualizacaja.do" method="post">
                <p>¿Desea hacer cambios a esta busqueda?</p>
                Si <input type="radio" onclick="javascript:yesnoCheck();" name="yesno" id="yesCheck"> No <input type="radio" onclick="javascript:yesnoCheck();" name="yesno" id="noCheck"><br>
                <div id="ifYes" style="display:none"><table>

                        <tr>
                            <td></td>
                            <td>
                                <%                                out.println(" <input type=\"text\" id=\"id_Transaccion\" name=\"Nume_Salas\" value=\"" + sa.getNume_Salas() + "\"> ");
                                %>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input class="btn btn-success" id="aceptar" name="aceptar" type="submit" value="ENVIAR">
                            </td>
                        </tr>

                        <tr></tr>
                        <tr></tr>
                        <tr></tr>
                    </table>
                </div>
            </form>
        </div>

    </body>
</html>
