<%-- 
    Document   : consultapelicula
    Created on : 21/05/2017, 11:04:19 AM
    Author     : Cesar Gutierrez
--%>

<%@page import="modeloDTO.TablAsientosDTO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href="css/bootstrap.css" rel="stylesheet">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Resultados de Busqueda</title>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script type="text/javascript">

            function yesnoCheck() {
                if (document.getElementById('yesCheck').checked) {
                    document.getElementById('ifYes').style.display = 'block';
                } else
                    document.getElementById('ifYes').style.display = 'none';
            }
        </script>


    </head>
    <body>
        <style>
            @import "http://fonts.googleapis.com/css?family=Roboto:300,400,500,700";

            .container { margin-top: 20px; }
            .mb20 { margin-bottom: 20px; } 

            hgroup { padding-left: 15px; border-bottom: 1px solid #ccc; }
            hgroup h1 { font: 500 normal 1.625em "Roboto",Arial,Verdana,sans-serif; color: #2a3644; margin-top: 0; line-height: 1.15; }
            hgroup h2.lead { font: normal normal 1.125em "Roboto",Arial,Verdana,sans-serif; color: #2a3644; margin: 0; padding-bottom: 10px; }

            .search-result .thumbnail { border-radius: 0 !important; }
            .search-result:first-child { margin-top: 0 !important; }
            .search-result { margin-top: 20px; }
            .search-result .col-md-2 { border-right: 1px dotted #ccc; min-height: 140px; }
            .search-result ul { padding-left: 0 !important; list-style: none;  }
            .search-result ul li { font: 400 normal .85em "Roboto",Arial,Verdana,sans-serif;  line-height: 30px; }
            .search-result ul li i { padding-right: 5px; }
            .search-result .col-md-7 { position: relative; }
            .search-result h3 { font: 500 normal 1.375em "Roboto",Arial,Verdana,sans-serif; margin-top: 0 !important; margin-bottom: 10px !important; }
            .search-result h3 > a, .search-result i { color: #248dc1 !important; }
            .search-result p { font: normal normal 1.125em "Roboto",Arial,Verdana,sans-serif; } 
            .search-result span.plus { position: absolute; right: 0; top: 126px; }
            .search-result span.plus a { background-color: #248dc1; padding: 5px 5px 3px 5px; }
            .search-result span.plus a:hover { background-color: #414141; }
            .search-result span.plus a i { color: #fff !important; }
            .search-result span.border { display: block; width: 97%; margin: 0 15px; border-bottom: 1px dotted #ccc; }
        </style>

        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="/CinePlace/index.html">CinePlace</a>
                </div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="/CinePlace/menu.jsp">Ir a Menu </a></li>
                </ul>
            </div>
        </nav>
        <div class="container">
            <div class="container">
                <hgroup class="mb20">
                    <h3 style="text-align:center;">Numero de  ID</h3>
                    <h2 style="text-align:center;" class="lead"><strong class="text-danger">
                            <%
                                TablAsientosDTO asientos = (TablAsientosDTO) session.getAttribute("todosasientos");
                                out.println("<p>" + asientos.getId_Asientos() + "</p>");
                                out.println(" <input type=\"text\" id=\"id\" name=\"Id_Asientos\" value=\" " + asientos.getId_Asientos() + " \" hidden=\"true\"> ");
                            %>
                        </strong> </h2>							
                </hgroup>

                <section class="col-xs-12 col-sm-6 col-md-12">
                    <article class="search-result row">
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <a class="thumbnail"><img src="https://ak3.picdn.net/shutterstock/videos/9097148/thumb/10.jpg?i10c=img.resize(height:160)" alt="tGwTDT" /></a>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-2">
                            <ul class="meta-search">
                                <li><i class="glyphicons glyphicons-list-numbered"></i> <span>Numero del Asiento<br />&emsp;&emsp;&emsp;&emsp;<%out.println(asientos.getNum_Asiento());%></span></li>
                                <li><i class="glyphicons glyphicons-align-left"></i><span>Letra del Asientoo<br />&emsp;&emsp;&emsp;&emsp;<%out.println(asientos.getLetr_Asiento());%></span></li>
                               
                            </ul>
                        </div>

                        <span class="clearfix borda"></span>
                    </article>
                </section>                     
            </div>
        </div>

    </body>
</html>
